import React from "react";
import Banner from "./Banner";
import Items from "./Items";
const Body = () => {
  return (
    <div>
      <Banner />
      <Items />
    </div>
  );
};

export default Body;
