import React from "react";
import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";
import "../SCSS/index.scss";
const Main = () => {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
};

export default Main;
